# -*- coding: utf-8 -*-

class BBException(Exception):
    def __str__(self):
        return 'bitbucket error:\n  %s %s' % (self.__class__.__name__,
            Exception.__str__(self))



class RequestError(BBException):
    pass


class InvalidCredentials(BBException):
    pass


class RepositoryAlreadyExists(BBException):
    pass


class RepositoryNotFound(BBException):
    pass
