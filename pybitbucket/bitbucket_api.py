# -*- coding: utf-8 -*-
import requests
from bbexceptions import *
from time import strptime
from datetime import datetime

VERSION = "0.1"
BB_HTTP_BASE = 'https://{credentials}bitbucket.org/{owner}/{repo}/'
BB_SSH_BASE = 'ssh://git@bitbucket.org/{owner}/{repo}/'
API_BASE = 'https://api.bitbucket.org'
KINDS = ('bug', 'task', 'enhancement', 'proposal')
PRIORITYS = ('critial', 'blocker', 'major', 'minor', 'trivial')
ISSUES_STATUS = ('new', 'open', 'on hold', 'resolved', 'duplicate', 'wontfix')


def to_datetime(date_str):
    return datetime(*strptime(date_str, '%Y-%m-%d %H:%M:%S+00:00')[:7])


def bb_http_uri(**kwargs):
    uri = BB_HTTP_BASE.format(**kwargs)
    return uri


def bb_ssh_uri(**kwargs):
    uri = BB_SSH_BASE.format(**kwargs)
    return uri


class MetaData(object):
    '''{u'kind': u'task', u'version': None,
    u'component': u'Clickarvore', u'milestone': None}'''
    def __init__(self, issue, kind='bug', version=None, component=u'', milestone=None, **kwargs):
        self.issue = issue
        self.kind = kind in KINDS and kind or 'bug'
        self.version = version
        self.component = component
        self.milestone = milestone

    def get_dict(self):
        return {u'kind': self.kind, u'version': self.version,
            u'component': self.component, u'milestone': self.milestone}

    def __repr__(self):
        return "<MetaData: %s, %s>" % (self.kind, self.component)


class UserInfo(object):
    '''{"username": "tutorials",
    "first_name": "",
    "last_name": "",
    "is_team": false,
    "avatar": "https://secure.gravatar.com/avatar/0bc5bd490000b8e63c35c0f54e667b9e?d=identicon&s=32",
    "resource_uri": "/1.0/users/tutorials"}'''
    def __init__(self, bb, username=u'', first_name=u'', last_name=u'', is_team=False, avatar=u'', resource_uri=u'', **kwagrs):
        self.bb = bb
        self.username = username
        self.first_name = first_name
        self.last_name = last_name
        self.is_team = is_team
        self.avatar = avatar
        self.resource_uri = resource_uri

    def __repr__(self):
        return "<UserInfo: %s>" % self.username


class Comment(object):
    '''{"content": "This is a comment on the issue",
        "author_info": {
            "username": "tutorials",
            "first_name": "",
            "last_name": "",
            "is_team": false,
            "avatar": "https://secure.gravatar.com/avatar/0bc5bd490000b8e63c35c0f54e667b9e?d=identicon&s=32",
            "resource_uri": "/1.0/users/tutorials"
        },
        "comment_id": 1727910,
        "utc_updated_on": "2012-07-29 03:01:16+00:00",
        "utc_created_on": "2012-07-29 03:01:16+00:00",
        "is_spam": false}'''
    def __init__(self, issue, content=u'', author_info=None, comment_id=None, utc_updated_on=None, utc_created_on=None,
        is_spam=False, convert_markup=False, **kwagrs):
        self.issue = issue
        self.content = content
        self.author_info = type(author_info) == dict and UserInfo(self.issue.repository.bb, **author_info) or author_info
        self.comment_id = comment_id
        self.utc_updated_on = utc_updated_on and to_datetime(utc_updated_on) or None
        self.utc_created_on = utc_created_on and to_datetime(utc_created_on) or None
        self.is_spam = is_spam
        self.convert_markup = convert_markup
        self.resource_uri = '/1.0/repositories/%s/%s/issues/%s/comments/%s' % (self.issue.repository.bb.username,
                self.issue.repository.slug, self.issue.local_id, self.comment_id)

    def save(self):
        if self.comment_id:
            # PUT https://api.bitbucket.org/1.0/repositories/{accountname}/{repo_slug}/issues/{issue_id}/comments/{comment_id}
            dic = {'content': self.content}
            r = requests.put('%s%s' % (API_BASE, self.resource_uri),
                dic, auth=(self.issue.repository.bb.username, self.issue.repository.bb.password))
            if r.ok:
                return Comment(self.issue, **r.json())
        else:
            # POST https://api.bitbucket.org/1.0/repositories/{accountname}/{repo_slug}/issues/{issue_id}/comments
            self.resource_uri = self.resource_uri[:-5]
            if not self.content:
                raise "content é um atributo obrigatório"
            dic = {'content': self.content}
            r = requests.post('%s%s' % (API_BASE, self.resource_uri),
                dic, auth=(self.issue.repository.bb.username, self.issue.repository.bb.password))
            if r.ok:
                return Comment(self.issue, **r.json())
        raise RequestError(r.status_code)

    def delete(self):
        pass

    def __repr__(self):
        return "<Comment: %s, %s>" % (self.comment_id, str(self.author_info))


class Issue(object):
    def __init__(self, repository, status=u'', priority=u'', title=u'', reported_by=None, utc_last_updated=None, comment_count=0,
        metadata=None, content=None, created_on=u'', local_id=0, follower_count=0, utc_created_on=None, resource_uri=u'',
        is_spam=False, responsible=None, **kwagrs):
        '''{u'status': u'resolved',
            u'content': u'Reimportar dados de custos "',
            u'title': u'New Click',
            u'reported_by': {u'username': u'mra_br',
                u'first_name': u'',
                u'last_name': u'',
                u'is_team': False,
                u'avatar': u'https://secure.gravatar.com/avatar/fdccfd67a8f9f2efc5bba8ca?d=httpsFdwz7u9tu8usb.cloudfront.netab16d05user_blue.png&s=32',
                u'resource_uri': u'/1.0/users/mra_br'
            }, u'utc_last_updated': u'2012-11-01 12:50:04+00:00',
            u'responsible': {u'username': u'luizvita',
                u'first_name': u'Lui',
                u'last_name': u'Vita',
                u'is_team': False,
                u'avatar': u'https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2010/Sep/11/my_avatar.jpg',
                u'resource_uri': u'/1.0/users/luivita'},
            u'created_on': u'2012-10-31 20:15:49',
            u'priority': u'blocker',
            u'comment_count': 3,
            u'local_id': 357,
            u'is_spam': False,
            u'follower_count': 1,
            u'utc_created_on': u'2012-10-31 19:15:49+00:00',
            u'resource_uri': u'/1.0/repositories/zc/sos/issues/37',
            u'metadata': {u'kind': u'task',
                u'version': None,
                u'component': u'Clickarvore',
                u'milestone': None}
            }'''
        self.repository = repository
        self.status = status
        self.reported_by = type(reported_by) == dict and UserInfo(self.repository.bb, **reported_by) or reported_by
        self.title = title
        self.utc_last_updated = utc_last_updated and to_datetime(utc_last_updated) or None
        self.comment_count = comment_count
        self.metadata = type(metadata) == dict and MetaData(self, **metadata) or metadata
        self.content = content
        self.created_on = created_on
        self.local_id = local_id
        self.priority = priority
        self.follower_count = follower_count
        self.utc_created_on = utc_created_on and to_datetime(utc_created_on) or None
        self.resource_uri = resource_uri
        self.responsible = type(responsible) == dict and UserInfo(self.repository.bb, **responsible) or responsible

    def get_comments(self):
        # https://api.bitbucket.org/1.0/repositories/{accountname}/{repo_slug}/issues/{issue_id}/comments
        r = requests.get('%s%s/comments' % (API_BASE, self.resource_uri),
            auth=(self.repository.bb.username, self.repository.bb.password))
        if r.ok:
            return [Comment(self, **comment) for comment in r.json()]
        raise RequestError(r.status_code)

    def get_comment(self, comment_id):
        # https://api.bitbucket.org/1.0/repositories/{accountname}/{repo_slug}/issues/{issue_id}/comments/{comment_id}
        r = requests.get('%s%s/comments/%s' % (API_BASE, self.resource_uri, comment_id),
            auth=(self.repository.bb.username, self.repository.bb.password))
        if r.ok:
            return Comment(self, **r.json())
        raise RequestError(r.status_code)

    def get_followers(self):
        # https://api.bitbucket.org/1.0/repositories/{accountname}/{repo_slug}/issues/{issue_id}/followers
        r = requests.get('%s%s/followers' % (API_BASE, self.resource_uri),
            auth=(self.repository.bb.username, self.repository.bb.password))
        if r.ok:
            json_response = r.json()
            if 'count' in json_response:
                if json_response['count'] > 0:
                    return [UserInfo(self.repository.bb, **follower) for follower in json_response['followers']]
        raise RequestError(r.status_code)

    def save(self):
        if self.local_id:
            # PUT https://api.bitbucket.org/1.0/repositories/{accountname}/{repo_slug}/issues/{issue_id}
            dic = {'title': self.title, 'content': self.content}
            dic['responsible'] = self.responsible.username
            dic['priority'] = self.priority
            dic['status'] = self.status
            dic.update(self.metadata.get_dict())
            r = requests.put('%s%s' % (API_BASE, self.resource_uri),
                dic, auth=(self.repository.bb.username, self.repository.bb.password))
            if r.ok:
                return Issue(self.repository, **r.json())
        else:
            # POST https://api.bitbucket.org/1.0/repositories/{accountname}/{repo_slug}/issues
            if not self.title or not self.content:
                raise "title e content são atributos obrigatórios"
            dic = {'title': self.title, 'content': self.content}
            if self.responsible:
                if self.responsible.username:
                    dic['responsible'] = self.responsible.username
            if self.priority:
                dic['priority'] = self.priority
            if self.metadata:
                dic.update(self.metadata.get_dict())
            r = requests.post('%s/1.0/repositories/%s/%s/issues' % (API_BASE, self.repository.bb.username, self.repository.slug),
                dic, auth=(self.repository.bb.username, self.repository.bb.password))
            if r.ok:
                return Issue(self.repository, **r.json())
        raise RequestError(r.status_code)

    def delete(self):
        pass

    def __repr__(self):
        return u"<Issue: %s, %s>" % (self.title, self.status)


class Repository(object):
    def __init__(self, bb, owner=u'', scm=u'', slug=u'', is_private=False, name=u'', **kwagrs):
        self.bb = bb
        self.owner = owner
        self.scm = scm
        self.slug = slug
        self.is_private = is_private
        self.name = name
        self.resource_uri = '/1.0/repositories/%s/%s/' % (self.owner, self.slug)

    def get_clone_url(self, protocol):
        if protocol.lower() == 'http':
            return self.get_http_uri()
        return self.get_ssh_uri()

    def get_http_uri(self):
        kargs = dict(credentials='', owner=self.owner, repo=self.name)
        if self.is_private:
            kargs['credentials'] = '{0}:{1}@'.format(self.bb.username, self.bb.password)
        return bb_http_uri(**kargs)

    def get_ssh_uri(self):
        kargs = dict(owner=self.owner, repo=self.name)
        return bb_ssh_uri(**kargs)

    def get_issues(self, status=None):
        # https://api.bitbucket.org/1.0/repositories/znc/sos-gerencial/issues
        query_string = '?'
        if status:
            query_string += 'status=%s' % ISSUES_STATUS[status]

        r = requests.get('%s%sissues%s' % (API_BASE, self.resource_uri, query_string), auth=(self.bb.username, self.bb.password))
        if r.ok:
            json_response = r.json()
            if 'count' in json_response:
                if json_response['count'] > 0:
                    return [Issue(self, **issue) for issue in json_response['issues'] if issue]
                return []
        raise RequestError(r.status_code)

    def get_issue(self, local_id):
        r = requests.get('%s%sissues/%s' % (API_BASE, self.resource_uri, local_id), auth=(self.bb.username, self.bb.password))
        if r.ok:
            return Issue(self, **r.json())

    def get_branches(self):
        pass

    def get_followers(self):
        # https://api.bitbucket.org/1.0/repositories/{accountname}/{repo_slug}/followers
        r = requests.get('%s%sfollowers' % (API_BASE, self.resource_uri), auth=(self.bb.username, self.bb.password))
        if r.ok:
            json_response = r.json()
            if 'count' in json_response:
                if json_response['count'] > 0:
                    return [UserInfo(self.bb, **follower) for follower in json_response['followers']]
        raise RequestError(r.status_code)

    def save(self):
        pass

    def delete(self):
        pass

    def __repr__(self):
        return u"<Repository: %s, %s>" % (self.slug, self.owner)


class BitbucketApi(object):
    def __init__(self, username=u'', password=u''):
        self.username = username
        self.password = password

    def get_repository(self, slug_name=u''):
        r = requests.get('%s/1.0/user/repositories' % API_BASE, auth=(self.username, self.password))
        if r.ok:
            json_response = r.json()
            for repo in json_response:
                if repo['slug'] == slug_name:
                    return Repository(self, **repo)
            return RepositoryNotFound("%s/%s" % (self.username, slug_name))
        raise RequestError(r.status_code)

    def get_repositories(self):
        r = requests.get('%s/1.0/user/repositories' % API_BASE, auth=(self.username, self.password))
        if r.ok:
            json_response = r.json()
            return [Repository(self, **repo) for repo in json_response]
        raise RequestError(r.status_code)

    def __repr__(self):
        return '<BitbucketApi: Username Password>'
